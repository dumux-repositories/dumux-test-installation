The purpose of this repository is to automatically test the installation
scripts provided in [DuMuX](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux)
using GitLab CI.
